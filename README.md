# scumwiz
Perl script for automated NetHack startscumming

Requirements: SSH, GNU Screen & Perl incl. perl modules List::MoreUtils and Time::HiRes.
I really need to write a more detailed readme, but scumwiz.pl now has a fairly detailed
usage text if you run it with the flag -h or -help

This will explain how to run it, and some things you need to have set up in your rcfile.